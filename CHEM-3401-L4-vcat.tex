\title{
Synthesis of a \ch{VO(acac)2} catalyst and optimisation of reaction conditions for catalysed
oxidation of anthracene to anthraquinone}
\author{Ariel Lisogorsky \\ 3061877}
\date{CHEM - 3401 L \\ April 3, 2018}

\documentclass[12pt,titlepage,final]{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=0.85in]{geometry}
\usepackage{indentfirst}
\usepackage{chemfig}
\usepackage{chemmacros}
\usepackage{caption}
\usepackage{achemso}
\usepackage{microtype}
\usepackage{amsmath}
\usepackage[section]{placeins}
\usepackage{varioref}
\usepackage[capitalise,noabbrev]{cleveref}
\usepackage{booktabs}
\usepackage[obeyFinal]{todonotes}
\usepackage{tikz}
\usepackage{lscape}

\setkeys{acs}{articletitle = true}
\usechemmodule{units,redox,reactions}
\chemsetup{redox/pos=side}

\renewcommand*{\thefootnote}{\alph{footnote}} %letters instead of numbers for footnotes

\sisetup{
  round-half      = even,
  table-auto-round,
  multi-part-units= single,
  list-units      = single,
  range-units     = single
}

\captionsetup[figure]{width=0.8\linewidth}
\captionsetup[table]{width=0.8\linewidth}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%-------------------------------------------------------------------------%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\maketitle{}
\listoftodos{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
A 5-coordinate vanadyl acetylacetonate complex (\ch{VO(acac)2}) was synthesised. Reactivity and solubility of the complex was characterised in aqueous and organic solvent solutions. An
IR spectrum was used to confirm the identity of the complex formed. Parameters for a catalysed
oxidation of anthracene to anthraquinone by hydrogen peroxide were varied in order to determine
trends in turnover number and reaction yield. Handling losses during purification were found to
have greatest negative impact on yield. Purity (denoted by lower likelihood  of requirement for
additional purification steps), TON and yield appeared to increase with reflux temperature.
Yields for oxidation were found to be between 1\% and 70\%. TONs were found to range from
0.50 to 29.54. Lesser concentrations of catalyst in the reaction occasionally resulted in 
higher TON with lesser overall yield. Insufficient replication of trials resulted in large
variability.
\end{abstract}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

Many organic reactions can be catalysed by certain inorganic complexes or species. Especially
useful are chelated metals which are often soluble in organic solvents
\cite{Shriver2014Inorganic,Buffie2018Inorganic}, present as active sites in biomolecules and
enzymes\cite{Voet2016Fundamentalsa,Walker2016Principles} and even associated to polymer 
surfaces\cite{Shikov1992LiquidPhase,Yokoyama1985Catalytic}. 
Catalysts can often enable reactions to occur which
normally may have activation energies too high to feasibly reach. Additionally, certain
catalyst properties such as the chelating ligand\cite{Vlckova1987Measurement}, associated
polymer\cite{Shikov1992LiquidPhase} or enzymatic environment\cite{Walker2016Principles}.
Since catalyst species are regenerated after reaction, each molar equivalent present may undergo
reaction more than once, the average number of reactions catalysed per molecule is referred to
as the turnover number (TON). 

Catalytic oxidations of organic compounds often rely on formation and temporary stabilization
of a reactive oxygen species such as a peroxide (\ch{R-O-O}), or an oxygen radical species
\cite{Walker2016Principles,Shikov1992LiquidPhase,Yokoyama1985Catalytic} and result in formation
of alcohols, ketone and epoxides. Five coordinate oxovanadium complexes tend to catalyse the
formation of epoxides through what is believed to be a peroxide intermediate
\cite{Yokoyama1985Catalytic,Shikov1992LiquidPhase}. Vanadyl acetylacetonate has
been found to catalyse the oxidation of anthracene to anthraquinone with hydrogen peroxide
(\ch{H2O2})\cite{Buffie2018Inorganic,Charleton2011Coordination,Shikov1992LiquidPhase}.

An increase in temperature often increases the rate of reactions with high activation energies.
Temperatures that are too high however may cause undesired reactions to occur and destabilize
required intermediate species once again lowering rate. Similarly yield and catalyst utilization
efficiency should be affected by the concentrations of the reactants and catalyst present as well 
as the aforementioned temperature effects.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Materials and Method}
%%%%%%%%%%%%%%%%%%%%%
	\begin{table}[h]
		\centering
		\caption{Materials used in experiment and sources. Note: U of W denotes stocks 
			prepared/provided by the university of Winnipeg}
		\label{tbl:mat}
			\begin{tabular}{llcl} \toprule %
			%%-------------------------------------------------------------------------------%
			Name / Formula 			& CAS No. 		& Supplier 		& Appearance \\ \midrule %
			%%-------------------------------------------------------------------------------%
			\ch{VOSO4 * x H2O}		
				& 123334-20-3	& Sigma-Aldrich	& blue crystalline solid \\
			acetylacetone 
				& 123-54-6 		& Sigma-Aldrich & transparent liquid \\
			saturated \ch{Na2CO3} solution
				& 				& U of W 		& colourless transparent solution \\
			anthracene
				& 120-12-7		& Sigma-Aldrich & white crystalline powder \\
			\SI{30}{\percent} hydrogen peroxide
				& 7722-84-1		& Anachemia		& transparent liquid \\
			ethyl acetate
				& 				& U of W 		& transparent liquid \\
			chloroform
				& 				& U of W 		& transparent liquid \\
			methanol
				& 				& U of W 		& transparent liquid \\
			pyridine
				& 				& U of W 		& transparent liquid \\
			tetrahydrofuran (THF)
				& 				& U of W 		& transparent liquid \\
			ammonia solution
				& 				& U of W 		& transparent liquid \\
			\bottomrule%
			%%-------------------------------------------------------------------------------%	
			\end{tabular}
	\end{table}
%%%%%%%%%%%%%%%%%

\subsection{Synthesis of Vanadyl Acetylacetonate}

A \SI{1.049}{\g} portion of vanadyl sulfate hydrate was added to \SI{15}{\mL} of water and stirred 
until the crystals were mostly dissolved to form a blue solution. The solution darkened and
the remaining solid crystals dissolved on addition of \SI{1.060}{\g} of \iupac{2,4-pentadione}
to the solution. Dropwise addition of saturated sodium carbonate solution resulted in vigorous
evolution of gas, short-lived brown colouration at the site of contact and formation of a blue-green
precipitate. The intensity of evolution decreased as addition progressed. Addition was continued
until gas evolution stopped requiring approximately \SI{12}{\mL} in total. The mixture was vaccum
filtered and the faint brown-green filtrate was discarded. Once dry the remaining solid was a
bright aquamarine blue. A total of \SI{1.1584}{\g} of product was collected from the synthesis.

A small quantity of product was mixed with potassium bromide, ground and pressed to produce a
\SI{1}{\cm} thick pellet. A Bruker alpha IR spectrophotometer was used to determine an absorption
spectrum for the product (\Cref{fig:spectrum}). Spectrum bands corresponding to stretching and
twisting modes in the \ch{CH3} ends and a \ch{V=O} stretching mode in the compound were identified.

\subsection{Catalytic Oxidation of Anthracene}

A mixture was prepared by combining \SI{0.0250}{\g} of the previously synthesised vanadyl 
acetylacetonate catalyst with \SI{0.0511}{\g} of anthracene in \SI{20.0}{\mL} of ethyl acetate.
\SI{30}{\percent} hydrogen peroxide solution was added to the mixture resulting in separation of
aqueous and organic phases in solution. The less dense (top) phase developed an orange-red tint.
The mixture was vigorously stirred and allowed to reflux at \SI{75}{\celsius} for ninety minutes
As heating progressed, the colouration in the top phase rapidly faded to pale yellow and slowly
re-intensified over the course of the reaction. 

After two hours elapsed, \SI{50}{\mL} of water was used to wash the mixture in to a separatory
funnel. A pale yellow aqueous solution and a bright orange organic solution were separated. The 
aqueous solution was washed with \SI{10}{\mL} of chloroform and separated again. The remaining
water was finally washed with an additional \SI{2}{\mL} aliquot of chloroform and separated. The
organic layers were collected and left to evaporate while the colourless aqueous solution was
discarded.

After the solvent was evaporated, the remaining burnt-orange crystalline solid was re-dissolved
in \SI{21}{\mL} of toluene to form a cloudy orange mixture. The mixture was stirred for 10 minutes
and filtered. \SI{0.1719}{\g} of clay-orange product was collected. A small amount of product was
dissolved in chloroform and used to create a TLC plate alongside anthracene and anthraquinone
standards (also in chloroform solutions). Spots with Rf values corresponding to both anthracene ~%
($\approx 1$) and anthraquinone~(0.4-0.5) were observed in the product tested. A second toluene
wash and filtration was performed as above and \SI{0.0881}{\g} of product was recovered. The
product collected after the second washing only showed one spot on the TLC plate with an Rf
matching that of the anthraquinone standard.

Replicate trials with varied parameters for the reaction were performed as shown in 
\Vref{tbl:methds} achieving different yields.

\subsection{Characterisation of Vanadyl Acetylacetonate Solutions}

The behaviour vanadyl acetylacetonate complex synthesised in a variety of solutions was also
visually investigated. When dissolved in chloroform, methanol, pyridine, THF, and aqueous ammonia
the complex produced bright blue, green-yellow, yellow-green, grey-blue, and opaque orange
solutions. The vanadyl compound did not dissolve in pure saturated sodium carbonate solution on its
own but slowly formed a brown haze in solution when a the compound was added in chloroform solution.
(\Cref{tbl:solvents})

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Observations and Results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The \SI{1.1584}{\g} of catalyst synthesized represents a
\SI{82.53}{\percent} yield (shown in \cref{eq:voacacyeld}. 
The IR spectrum collected of the product (\Cref{fig:spectrum}) closely
matches the spectra described
\cite{dos_santos_claro_paula_c._spectroscopic_2005,Nakamoto1961Infrared}
and present in literature databases\bibnote{CAS SciFinder}.

%%%%%%%%%%%%%%%%%%%%
\begin{figure}[hbtp]
	\centering
	\input{Plots/spec.tex}
	\caption{IR absorption spectrum of \ch{VO(acac)2} catalyst in a \ch{KBr} pellet from bruker
	alpha IR spectrophotometer. Selected stretching regions labelled above spectrum}
	\label{fig:spectrum}
\end{figure}
%%%%%%%%%%%%%%%%%%%%

Dissolution of the produced catalyst in various solvents resulted in solutions of differing colour.
\Vref{tbl:solvents} summarizes the colours of solution resulting from dissolution of the
synthesised complex. The complex was initially insoluble in aqueous \ch{NaCO3} but reacted with the
solution when first dissolved in \ch{CH2Cl2}.

%%%%%%%%%%%%%%%%%%%%
	\begin{table}[htbp]
	\centering
	\caption{Colour of solutions formed by dissolving vanadyl acetylacetonate in various solvents.
	compound was insoluble in \ch{Na2CO3}}
	\label{tbl:solvents}
		\begin{tabular}{lc} \toprule%
		%%--------------------------%
		solvent(s)	& colour\\ \midrule
		%%--------------------------%
		\ch{CH2Cl2} & royal blue \\
		\ch{CH3OH}	& green-yellow \\
		pyridine	& yellow-green \\
		\ch{NH3 \aq{}} & cloudy orange \\
		\ch{Na2CO3 \aq{}} & N/A \\
		\ch{Na2CO3 \aq{} + CH2Cl2} & blue (organic) + brown (aqueous) \\
		\bottomrule%
		%%--------------------------%
	\end{tabular}
	\end{table}
%%%%%%%%%%%%%%%%%%%%

Oxidation of anthracene with the catalyst by reflux occasionally resulted in impurities that
remained after the first washing step observed as a second spot with an Rf near 1 on a TLC 
plate (\Cref{fig:tlc}) in some trials. In general, the presence of these impurities 
and therefore additional washes was the strongest determining values on yield. Yields varied
from \SIrange{1.70}{70.95}{\percent} in the trials conducted (\Cref{tbl:methds}. Calculated turnover numbers varied 
from 0.5 to 29.5 and loosely correlated with temperature. The first trial performed had a
yield of \SI{0.0881}{\g} after the second washing (\SI{15}{\percent}, \Cref{eq:Aqyield}) and therefore had a
calculated turnover number of 4.49 (\Cref{eq:TON}.

\begin{figure}[hbtp]
	\centering
	\includegraphics{Plots/TLCplates.eps}
	\caption{Schematic of thin layer chromatography (TLC) plates developed to determine purity of
	anthraquinone product after one (left) and two (right) wash and filtration steps with toluene.
	Ac, Aq and Prod denote spots produced by an anthracene standard, an anthraquinone standard and
	the product of the synthesis respectively. Rf values for anthracene and anthraquinone were
	found to be 1.0 and 0.5 respectively.}
	\label{fig:tlc}
\end{figure}
%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The synthesis method used to prepare the vanadyl acetylacetonate provided a good
yield (\SI{82.53}{\percent}). The product displayed a similar spectrum to both what was expected
and what was found in the literature suggesting reasonable purity. The product also successfully
catalysed the oxidation of anthracene to anthraquinone in conditions where the reaction provides
no substantial yield without presence of a catalyst\cite{Charleton2011Coordination}.

When dissolved in a variety of solvents, the vanadyl acetylacetonate complex produced solutions of
varied appearance. In some solutions, (exemplified by chloroform) the complex dissolved but
did not noticeably change in colour, this suggests that the majority of the complex remained 
structurally similar (in a 5-coordinate state). In other solvents, a discolouration from the
initial bright blue green likely indicated that some of the complex in solution had changed in
structure to a 6-coordinate state. This effect was most obviously observed in the aqueous solutions
tested. The 5-coordinate complex was not soluble in deionised water or a \ch{Na2CO3} solution but
became soluble in aqueous ammonia where it underwent a drastic colour change to form a milky orange
solution. Similarly, when initially dissolved in chloroform the complex underwent a slow reaction
to form a brown species that was soluble in the carbonate solution. Both the change in solubility and colour likely suggest that the complex shifted from a 5-coordinate to a 6-coordinate complex,
in aqueous solutions. Pyridine has also been reported to show a similar colour change in the
literature\cite{Charleton2011Coordination} but a much less drastic (but still noticeable) change
in colour was observed in the experiment.

In the various trials attempted to optimize the catalytic oxidation, the most consistent predictor
of yield was the number of washes performed. While this may imply experimental conditions
that led to greater content of anthracene\footnote{suggesting greater contaminant concentrations
and less product yield} it is also likely that the wash and filtration steps performed also
accounted for the greatest source of handling loss of product. Overall, an insufficient number
of replicate trials were performed to determine whether variation was due to deliberate or
incidental variation between trial methods. Overall, the expected temperature trend predicting
greater yield at higher temperatures appears to have merit with trials performed at higher
temperatures tending to be less likely to have impurities present in the product and
correspondingly higher yields.


With the extreme variation in yield observed, the turnover number values that were calculated
are not likely to be accurately representative of the true values for the catalyst in solution
as the value was calculated from yield and is therefore subject to the handling losses described
above. The likely disparity between the chemical yield of the reaction and the product recovery
suggest that the calculated turnover numbers are more likely to reflect the ability to recover
product rather than an empirical relationship between the reaction rate and reaction conditions.
Regardless, whether through chance, better recovery or a truly increased reaction rate, reactions
performed at higher temperatures appear to show increased turnover numbers for the catalyst and
therefore faster reaction rates. Conversely, the effect of decreased temperature appears to be
very pronounced on the quantity of product produced with minimal yield being achieved at the
lowest temperature.

In all it appears that individual variation in trials was the largest source of disparity with
perceived product purity\footnote{and therefore number of washes} as the greatest determining
factor of yield\footnote{actually representing recovery of product rather than the yield of the reaction.} and the perceived turnover number in the reaction.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Appendix}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	\subsection{Reactions}
	\begin{reactions}
	\intertext{Carbonate Acid-base equilibrium}
	CO3^{2-} + H2O &<-> HCO3- + OH- \\
	HCO3- + H2O &<-> H2CO3 + OH- \\
	H2CO3 \aq{} &<-> H2O \lqd{} + CO2 \gas{} \\
	\intertext{Production of acetylacetonate (acac-) ion in basic solution}
	H3C-C(=O)-CH2-C(=O)-CH3 + OH- &-> [H3C-C(=O)-CH3-C(=O)-CH3]- + H2O 
	\label{rct:acacprod}
	\intertext{Formation of oxovanadium complex with acac-}
	VO^{2+}  \aq{} + 2 acac- \aq{} &-> VO(acac)2 \sld{} \\
	\intertext{Formation of 6-coordinate soluble complex (ex. Aqueous Ammonia)}
	VO(acac)2 \sld{} + NH3 \aq{} &-> [VO(acac)2(NH3)] \aq{}
	\end{reactions}

	\subsection{Sample Calculations}
	\begin{align}
	\intertext{Yield calculations for catalyst}
	\SI{1.049}{g_{VO}} \times \frac{\SI{1}{\mol}}{\SI{163.0}{\g}} \times \SI{265.167}{\g\per\mol}
	& = \SI{1.4035}{\g} \\
	\SI{1.060}{g_{VO}} \times \frac{\SI{1}{\mol}}{\SI{100.13}{\g}} \times 
	\frac{\SI{1}{mol_{acac}}}{\SI{1}{mol_{VO(acac)_2}}} \times \SI{265.167}{\g\per\mol}
	& = \SI{1.706}{\g} \\
	\frac{\SI{1.4035}{\g}}{{\SI{1.1584}{\g}}} &= \SI{82.54}{\percent}
	\label{eq:voacacyeld}
	\intertext{Yield calculations for anthraquinone}
	\SI{0.5011}{g_{Ac}} \times \frac{\SI{1}{\mol}}{\SI{178.23}{\g}} \times \SI{208.22}{\g\per\mol}
	& = \SI{0.5854}{q_{Aq}} \\
	\frac{\SI{0.0881}{\g}}{{\SI{0.5854}{\g}}} &= \SI{15.05}{\percent}
	\label{eq:Aqyield}
	\intertext{Turnover Number (TON)}
	\frac{\SI{0.5011}{g}}{\SI{173.23}{\g\per\mol}} \times
	\frac{\SI{265.16}{\g\per\mol}}{\SI{0.0250}{\g}} \times 0.1505
	&= 4.4877 \label{eq:TON}
	\intertext{The above could have been calculated with Aq
	instead by using the appropriate molecular mass and not multiplying
	by the fractional yield (0.1505)}
	\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{landscape}
\begin{table}[p]
\centering
\caption{results of catalysed oxidation reflux method variations used in experiment. TON refers
to turnover number of the catalyst, the number of reactions catalysed on average per molecule of
catalyst in solution.}
\label{tbl:methds}
\include{resultstable}
\end{table}
\end{landscape}

\bibliography{Remote}


\end{document}